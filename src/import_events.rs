use std::cell::RefCell;
use std::str::from_utf8;

use anyhow::{ensure, Result};
use http_body_util::BodyExt;
use hyper::{body::Incoming, Request};
use memchr::memchr;

use crate::State;

pub async fn import_events(state: &RefCell<State>, request: Request<Incoming>) -> Result<()> {
    let mut body = request.into_body();

    let mut buffer = Vec::new();

    while let Some(frame) = body.frame().await.transpose()? {
        let data = frame.into_data().unwrap();

        if !buffer.is_empty() {
            buffer.extend(data);

            let consumed = parse_chunk(state, &buffer)?;

            if consumed < buffer.len() {
                buffer.copy_within(consumed.., 0);
                buffer.truncate(buffer.len() - consumed);
            } else {
                buffer.clear();
            }
        } else {
            let consumed = parse_chunk(state, &data)?;

            if consumed < data.len() {
                buffer.extend(&data[consumed..]);
            }
        };
    }

    Ok(())
}

fn parse_chunk(state: &RefCell<State>, chunk: &[u8]) -> Result<usize> {
    let mut chunk_consumed = 0;

    loop {
        match parse_line(&chunk[chunk_consumed..])? {
            Line::Incomplete => break,
            Line::Event => {
                state.borrow_mut().push_event()?;

                chunk_consumed += 1;
            }
            Line::Field {
                consumed,
                name,
                value,
            } => {
                state.borrow_mut().push_field(name, value)?;

                chunk_consumed += consumed + 1;
            }
        };
    }

    Ok(chunk_consumed)
}

enum Line<'a> {
    Incomplete,
    Event,
    Field {
        consumed: usize,
        name: &'a str,
        value: &'a [u8],
    },
}

fn parse_line(remaining: &[u8]) -> Result<Line> {
    match memchr(b'\n', remaining) {
        Some(0) => Ok(Line::Event),
        Some(newline) => {
            let line = &remaining[..newline];

            match memchr(b'=', line) {
                Some(equal) => {
                    let name = from_utf8(&line[..equal])?;
                    let value = &line[equal + 1..];

                    Ok(Line::Field {
                        consumed: newline,
                        name,
                        value,
                    })
                }
                None => {
                    let name = from_utf8(line)?;

                    let value = &remaining[newline + 1..];

                    if value.len() < 8 {
                        return Ok(Line::Incomplete);
                    }

                    let length = u64::from_le_bytes(value[..8].try_into().unwrap())
                        .try_into()
                        .unwrap();

                    let value = &value[8..];

                    if value.len() < length + 1 {
                        return Ok(Line::Incomplete);
                    }

                    ensure!(value[length] == b'\n');

                    let value = &value[..length];

                    Ok(Line::Field {
                        consumed: newline + 1 + 8 + length,
                        name,
                        value,
                    })
                }
            }
        }
        None => Ok(Line::Incomplete),
    }
}
