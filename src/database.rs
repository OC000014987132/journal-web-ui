use std::cell::Cell;
use std::ops::{Deref, DerefMut};
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use anyhow::Result;
use compact_str::CompactString;
use hashbrown::{HashMap, HashSet};
use mini_moka::sync::Cache;
use rusqlite::{params, Connection, OptionalExtension, Statement, TransactionBehavior};
use tokio::time::Instant;
use zstd::bulk::compress;

use crate::{
    utilities::{read_leb128, write_leb128},
    Event, Field, State, CACHE_CAPACITY,
};

pub fn init_database() -> Result<State> {
    let mut conn = Connection::open("logs.sqlite")?;

    conn.pragma_update(None, "journal_mode", "WAL")?;
    conn.pragma_update(None, "synchronous", "NORMAL")?;

    let txn = conn.transaction()?;

    let user_version: i32 = txn.pragma_query_value(None, "user_version", |row| row.get(0))?;

    if user_version < 1 {
        txn.execute_batch(
            r#"
CREATE TABLE events (
    id INTEGER PRIMARY KEY,
    timestamp INTEGER NOT NULL,
    host_id INTEGER,
    container_id INTEGER,
    unit_id INTEGER,
    fields BLOB
);

CREATE INDEX events_by_timestamp ON events (timestamp);

CREATE INDEX events_by_host ON events (host_id);

CREATE INDEX events_by_container ON events (container_id);

CREATE INDEX events_by_unit ON events (unit_id);

CREATE TABLE hosts (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);

CREATE TABLE containers (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);

CREATE TABLE units (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL UNIQUE
);

CREATE TABLE fields (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    data BLOB NOT NULL
);

PRAGMA user_version = 1;"#,
        )?;
    }

    let next_event_id =
        txn.query_row_and_then("SELECT COALESCE(MAX(id) + 1, 0) FROM events", [], |row| {
            row.get(0)
        })?;

    let next_field_id =
        txn.query_row_and_then("SELECT COALESCE(MAX(id) + 1, 0) FROM fields", [], |row| {
            row.get(0)
        })?;

    txn.commit()?;

    let cached_fields = Cache::builder()
        .max_capacity(CACHE_CAPACITY)
        .weigher(|_id, data: &Arc<[u8]>| data.len().try_into().unwrap())
        .build();

    Ok(State {
        curr_event: Default::default(),
        events: Vec::new(),
        fields: HashMap::new(),
        buffered: 0,
        last_flush: Instant::now(),
        next_event_id,
        next_field_id,
        cached_fields: Arc::new(cached_fields),
        expected_messages: None,
    })
}

pub fn write_events(events: Vec<Event>, fields: HashMap<CompactString, Field>) -> Result<()> {
    let mut conn = open_connection()?;
    let txn = conn.transaction_with_behavior(TransactionBehavior::Immediate)?;

    {
        let mut stmt = txn.prepare_cached("INSERT INTO events VALUES (?, ?, ?, ?, ?, ?)")?;

        let mut hosts_select_stmt = txn.prepare_cached("SELECT id FROM hosts WHERE name = ?")?;
        let mut hosts_insert_stmt = txn.prepare_cached("INSERT INTO hosts (name) VALUES (?)")?;

        let mut containers_select_stmt =
            txn.prepare_cached("SELECT id FROM containers WHERE name = ?")?;
        let mut containers_insert_stmt =
            txn.prepare_cached("INSERT INTO containers (name) VALUES (?)")?;

        let mut units_select_stmt = txn.prepare_cached("SELECT id FROM units WHERE name = ?")?;
        let mut units_insert_stmt = txn.prepare_cached("INSERT INTO units (name) VALUES (?)")?;

        let mut fields = Vec::<u8>::new();

        for event in events {
            let host_id = select_or_insert(
                &txn,
                &mut hosts_select_stmt,
                &mut hosts_insert_stmt,
                event.host.as_deref(),
            )?;

            let container_id = select_or_insert(
                &txn,
                &mut containers_select_stmt,
                &mut containers_insert_stmt,
                event.container.as_deref(),
            )?;

            let unit_id = select_or_insert(
                &txn,
                &mut units_select_stmt,
                &mut units_insert_stmt,
                event.unit.as_deref(),
            )?;

            fields.clear();

            for field in &event.fields {
                write_leb128(&mut fields, field.id);
                write_leb128(&mut fields, field.offset.try_into().unwrap());
            }

            stmt.execute(params![
                event.id,
                event.timestamp,
                host_id,
                container_id,
                unit_id,
                fields,
            ])?;
        }
    }

    {
        let mut stmt = txn.prepare("INSERT INTO fields VALUES (?, ?, ?)")?;

        for (name, field) in fields {
            let data = compress(&field.data, 17)?;

            stmt.execute(params![field.id, name.as_str(), data])?;
        }
    }

    txn.commit()?;

    Ok(())
}

pub fn prune_events(max_age: Duration) -> Result<()> {
    let mut conn = open_connection()?;
    let txn = conn.transaction_with_behavior(TransactionBehavior::Immediate)?;

    let deadline = (SystemTime::now() - max_age)
        .duration_since(SystemTime::UNIX_EPOCH)?
        .as_secs();

    txn.execute("DELETE FROM events WHERE timestamp < ?", params![deadline])?;

    let deleted = txn.changes();
    if deleted != 0 {
        println!("Pruned {} events", deleted);

        txn.execute(
            "DELETE FROM hosts WHERE id NOT IN (SELECT DISTINCT host_id FROM events)",
            [],
        )?;

        let deleted = txn.changes();
        if deleted != 0 {
            println!("Pruned {} hosts", deleted);
        }

        txn.execute(
            "DELETE FROM containers WHERE id NOT IN (SELECT DISTINCT container_id FROM events)",
            [],
        )?;

        let deleted = txn.changes();
        if deleted != 0 {
            println!("Pruned {} containers", deleted);
        }

        txn.execute(
            "DELETE FROM units WHERE id NOT IN (SELECT DISTINCT unit_id FROM events)",
            [],
        )?;

        let deleted = txn.changes();
        if deleted != 0 {
            println!("Pruned {} units", deleted);
        }

        let mut used_fields = HashSet::<u64>::new();

        {
            let mut stmt = txn.prepare("SELECT fields FROM events")?;
            let mut rows = stmt.query([])?;

            while let Some(row) = rows.next()? {
                let mut fields = row.get_ref_unwrap(0).as_blob()?;

                while !fields.is_empty() {
                    let id = read_leb128(&mut fields);
                    let _offset = read_leb128(&mut fields);

                    used_fields.insert(id);
                }
            }
        }

        let mut unused_fields = Vec::<u64>::new();

        {
            let mut stmt = txn.prepare("SELECT id FROM fields")?;
            let mut rows = stmt.query([])?;

            while let Some(row) = rows.next()? {
                let id = row.get(0)?;

                if !used_fields.contains(&id) {
                    unused_fields.push(id);
                }
            }
        }

        if !unused_fields.is_empty() {
            let mut stmt = txn.prepare("DELETE FROM fields WHERE id = ?")?;

            for id in &unused_fields {
                stmt.execute(params![id])?;
            }

            println!("Pruned {} fields", unused_fields.len());
        }
    }

    txn.commit()?;

    Ok(())
}

fn select_or_insert(
    conn: &Connection,
    select_stmt: &mut Statement,
    insert_stmt: &mut Statement,
    name: Option<&str>,
) -> Result<Option<i64>> {
    match name {
        Some(name) => {
            let id: Option<i64> = select_stmt
                .query_row(params![name], |row| row.get(0))
                .optional()?;

            match id {
                Some(id) => Ok(Some(id)),
                None => {
                    insert_stmt.execute(params![name])?;

                    Ok(Some(conn.last_insert_rowid()))
                }
            }
        }
        None => Ok(None),
    }
}

thread_local! {
    static CONN: Cell<Option<Connection>> = const { Cell::new(None) };
}

pub struct CachedConnection(Option<Connection>);

impl Deref for CachedConnection {
    type Target = Connection;

    fn deref(&self) -> &Self::Target {
        self.0.as_ref().unwrap()
    }
}

impl DerefMut for CachedConnection {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.0.as_mut().unwrap()
    }
}

impl Drop for CachedConnection {
    fn drop(&mut self) {
        let conn = self.0.take();

        CONN.set(conn);
    }
}

pub fn open_connection() -> Result<CachedConnection> {
    let conn = match CONN.take() {
        Some(conn) => conn,
        None => {
            let conn = Connection::open("logs.sqlite")?;
            conn.busy_timeout(Duration::from_secs(60))?;
            conn
        }
    };

    Ok(CachedConnection(Some(conn)))
}
