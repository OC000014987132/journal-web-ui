mod database;
mod import_events;
mod send_mail;
mod utilities;
mod view_events;

use std::cell::RefCell;
use std::env::var;
use std::hash::BuildHasher;
use std::mem::{size_of, take};
use std::process::exit;
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use anyhow::Result;
use compact_str::CompactString;
use hashbrown::{DefaultHashBuilder, HashMap, HashTable};
use humantime::parse_duration;
use hyper::{
    header::CONTENT_TYPE, server::conn::http1::Builder, service::service_fn, Method, Response,
    StatusCode,
};
use hyper_util::rt::tokio::{TokioIo, TokioTimer};
use mini_moka::sync::Cache;
use regex::RegexSet;
use tokio::{
    net::TcpListener,
    signal::unix::{signal, SignalKind},
    sync::{mpsc::UnboundedSender, oneshot::channel as oneshot_channel},
    task::{spawn_blocking, JoinHandle, LocalSet},
    time::{interval_at, Instant, MissedTickBehavior},
};

use database::{init_database, prune_events, write_events};
use import_events::import_events;
use send_mail::send_mail;
use utilities::{parse_timestamp, read_leb128, write_leb128};
use view_events::{format_event, view_events};

#[tokio::main(flavor = "current_thread")]
async fn main() -> Result<()> {
    let state = &*Box::leak(Box::new(RefCell::new(init_database()?)));

    let tasks = LocalSet::new();
    let listener = TcpListener::bind(var("BIND_ADDR")?).await?;

    tasks
        .run_until(async {
            let send_mail_flush_sender = send_mail(state)?;

            let flush_interval = match var("FLUSH_INTERVAL") {
                Ok(flush_interval) => parse_duration(&flush_interval)?,
                Err(_err) => FLUSH_INTERVAL,
            };

            tasks.spawn_local(async move {
                let mut interval = interval_at(Instant::now() + flush_interval, flush_interval);
                interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

                loop {
                    interval.tick().await;

                    let flush = {
                        let mut state = state.borrow_mut();

                        if state.events.is_empty() {
                            continue;
                        }

                        if Instant::now().duration_since(state.last_flush) < flush_interval {
                            continue;
                        }

                        state.flush()
                    };

                    flush.await.unwrap();
                }
            });

            let mut terminate = signal(SignalKind::terminate())?;

            tasks.spawn_local(async move {
                terminate.recv().await;

                if let Some(send_mail_flush_sender) = send_mail_flush_sender {
                    let (flush_sender, flush_receiver) = oneshot_channel();
                    let _ = send_mail_flush_sender.send(flush_sender).await;
                    let _ = flush_receiver.await;
                }

                let flush = {
                    let mut state = state.borrow_mut();

                    if state.events.is_empty() {
                        exit(0);
                    }

                    state.flush()
                };

                flush.await.unwrap();
                exit(0);
            });

            let max_age = match var("MAX_AGE") {
                Ok(max_age) => parse_duration(&max_age)?,
                Err(_err) => MAX_AGE,
            };

            let prune_interval = match var("PRUNE_INTERVAL") {
                Ok(prune_interval) => parse_duration(&prune_interval)?,
                Err(_err) => PRUNE_INTERVAL,
            };

            tasks.spawn_local(async move {
                let mut interval = interval_at(Instant::now(), prune_interval);
                interval.set_missed_tick_behavior(MissedTickBehavior::Skip);

                loop {
                    interval.tick().await;

                    spawn_blocking(move || {
                        if let Err(err) = prune_events(max_age) {
                            eprintln!("Failed to prune events: {:#}", err);
                        }
                    })
                    .await
                    .unwrap();
                }
            });

            loop {
                let (socket, _) = listener.accept().await?;

                tasks.spawn_local(async move {
                    if let Err(err) = Builder::new()
                        .timer(TokioTimer::new())
                        .header_read_timeout(None)
                        .serve_connection(
                            TokioIo::new(socket),
                            service_fn(|request| async move {
                                if request.method() == Method::POST {
                                    match import_events(state, request).await {
                                        Ok(()) => Response::builder()
                                            .status(StatusCode::NO_CONTENT)
                                            .body(String::new()),
                                        Err(err) => Response::builder()
                                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                                            .header(CONTENT_TYPE, "text/plain; charset=utf-8")
                                            .body(format!("{:#}", err)),
                                    }
                                } else {
                                    match view_events(state, request).await {
                                        Ok(messages) => Response::builder()
                                            .status(StatusCode::OK)
                                            .header(CONTENT_TYPE, "text/plain; charset=utf-8")
                                            .body(messages),
                                        Err(err) => Response::builder()
                                            .status(StatusCode::INTERNAL_SERVER_ERROR)
                                            .header(CONTENT_TYPE, "text/plain; charset=utf-8")
                                            .body(format!("{:#}", err)),
                                    }
                                }
                            }),
                        )
                        .await
                    {
                        eprintln!("Failed to serve connection: {}", err);
                    }
                });
            }
        })
        .await
}

struct State {
    curr_event: Event,
    events: Vec<Event>,
    fields: HashMap<CompactString, Field>,
    buffered: usize,
    last_flush: Instant,
    next_field_id: u64,
    next_event_id: u64,
    cached_fields: Arc<Cache<u64, Arc<[u8]>>>,
    expected_messages: Option<ExpectedMessages>,
}

struct ExpectedMessages {
    patterns: Arc<RegexSet>,
    sender: UnboundedSender<String>,
    curr_message: String,
    buffer: String,
}

impl State {
    fn push_event(&mut self) -> Result<()> {
        self.curr_event.id = self.next_event_id;
        self.next_event_id += 1;

        if self.curr_event.timestamp.is_none() {
            let timestamp = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)?
                .as_secs();

            self.curr_event.timestamp = Some(timestamp);
        }

        self.curr_event.fields.shrink_to_fit();

        if let Some(expected_messages) = &mut self.expected_messages {
            let timestamp = self.curr_event.timestamp.unwrap();
            let message = &expected_messages.curr_message;

            let buffer = &mut expected_messages.buffer;
            buffer.clear();

            let after_timestamp = format_event(
                buffer,
                timestamp,
                self.curr_event.host.as_deref(),
                self.curr_event.container.as_deref(),
                self.curr_event.unit.as_deref(),
                message,
            );

            if !expected_messages
                .patterns
                .is_match(&buffer[after_timestamp..])
            {
                let _ = expected_messages.sender.send(take(buffer));
            }
        }

        self.events.push(take(&mut self.curr_event));

        self.buffered += size_of::<Event>();

        if self.buffered > BUFFERED_LIMIT {
            self.flush();
        }

        Ok(())
    }

    fn push_field(&mut self, name: &str, value: &[u8]) -> Result<()> {
        let mut store_inline = || -> Result<_> {
            let mut value = CompactString::from_utf8(value)?;

            if value.is_heap_allocated() {
                value.shrink_to_fit();

                self.buffered += value.len();
            }

            Ok(Some(value))
        };

        match name {
            "__REALTIME_TIMESTAMP" => self.curr_event.timestamp = Some(parse_timestamp(value)?),
            "_HOSTNAME" => self.curr_event.host = store_inline()?,
            "CONTAINER_NAME" => self.curr_event.container = store_inline()?,
            "UNIT" => self.curr_event.unit = store_inline()?,
            name => {
                if name.starts_with("__") {
                    return Ok(());
                }

                if name == "SYSLOG_IDENTIFIER" && self.curr_event.unit.is_none() {
                    self.curr_event.unit = store_inline()?;
                }

                if let Some(expected_messages) = &mut self.expected_messages {
                    if name == "MESSAGE" {
                        expected_messages.curr_message =
                            String::from_utf8_lossy(value).into_owned();
                    }
                }

                let field = self.fields.entry_ref(name).or_insert_with(|| {
                    let id = self.next_field_id;
                    self.next_field_id += 1;

                    self.buffered += size_of::<Field>();

                    Field {
                        id,
                        data: Vec::new(),
                        offsets: HashTable::new(),
                    }
                });

                let (offset, buffered) = field.push_value(value);

                let field_value = FieldValue {
                    id: field.id,
                    offset,
                };

                self.curr_event.fields.push(field_value);

                self.buffered += buffered + size_of::<FieldValue>();
            }
        }

        Ok(())
    }

    fn flush(&mut self) -> JoinHandle<()> {
        let events = take(&mut self.events);
        let fields = take(&mut self.fields);

        self.buffered = 0;
        self.last_flush = Instant::now();

        println!(
            "Flushing {} events and {} fields...",
            events.len(),
            fields.len()
        );

        spawn_blocking(move || {
            if let Err(err) = write_events(events, fields) {
                eprintln!("Failed to write events: {:#}", err);
                exit(1);
            }
        })
    }
}

#[derive(Default)]
struct Event {
    id: u64,
    timestamp: Option<u64>,
    host: Option<CompactString>,
    container: Option<CompactString>,
    unit: Option<CompactString>,
    fields: Vec<FieldValue>,
}

struct FieldValue {
    id: u64,
    offset: usize,
}

struct Field {
    id: u64,
    data: Vec<u8>,
    offsets: HashTable<usize>,
}

impl Field {
    fn push_value(&mut self, value: &[u8]) -> (usize, usize) {
        let hasher = DefaultHashBuilder::default();
        let hash = hasher.hash_one(value);

        if let Some(offset) = self.offsets.find(hash, |offset| {
            let mut value_at_offset = &self.data[*offset..];
            let length = read_leb128(&mut value_at_offset).try_into().unwrap();
            let value_at_offset = &value_at_offset[..length];

            value_at_offset == value
        }) {
            return (*offset, 0);
        }

        let offset = self.data.len();

        write_leb128(&mut self.data, value.len().try_into().unwrap());

        self.data.extend(value);

        self.offsets
            .insert_unique(hash, offset, |value| hasher.hash_one(value));

        (offset, self.data.len() - offset + size_of::<usize>())
    }
}

const BUFFERED_LIMIT: usize = 1 << 25; // 32 MB

const FLUSH_INTERVAL: Duration = Duration::from_secs(15 * 60); // 15 min

const MAX_AGE: Duration = Duration::from_secs(90 * 24 * 60 * 60); // 90 d

const PRUNE_INTERVAL: Duration = Duration::from_secs(60 * 60); // 1 h

const CACHE_CAPACITY: u64 = 1 << 25; // 32 MB

const RESPONSE_LIMIT: usize = 1 << 20; // 1 MB

const SEND_INTERVAL: Duration = Duration::from_secs(15 * 60); // 15 min
