use std::cell::RefCell;
use std::fmt::Write;
use std::sync::Arc;
use std::time::{Duration, SystemTime};

use anyhow::{bail, ensure, Result};
use form_urlencoded::parse as parse_query;
use humantime::{format_rfc3339_seconds, parse_duration, parse_rfc3339_weak};
use hyper::{body::Incoming, Request};
use regex::Regex;
use rusqlite::{params, ToSql};
use tokio::task::spawn_blocking;
use zstd::bulk::decompress;

use crate::{database::open_connection, utilities::read_leb128, State, RESPONSE_LIMIT};

pub async fn view_events(
    state: &'static RefCell<State>,
    request: Request<Incoming>,
) -> Result<String> {
    let query = parse_query(request.uri().query().unwrap_or_default().as_bytes());

    let mut since = None;
    let mut until = None;

    let mut host = None;
    let mut container = None;
    let mut unit = None;

    let mut grep = None;

    let mut lines = None;
    let mut unexpected = None;

    let parse_time_filter = |value: &str| -> Result<u64> {
        let value = if let Some(value) = value.strip_prefix('-') {
            let value = parse_duration(value)?;
            SystemTime::now() - value
        } else {
            parse_rfc3339_weak(value)?
        };

        let value = value.duration_since(SystemTime::UNIX_EPOCH)?;

        Ok(value.as_secs())
    };

    for (key, value) in query {
        match &*key {
            "since" => since = Some(parse_time_filter(&value)?),
            "until" => until = Some(parse_time_filter(&value)?),

            "host" => host = Some(value.into_owned()),
            "container" => container = Some(value.into_owned()),
            "unit" => unit = Some(value.into_owned()),

            "grep" => grep = Some(Regex::new(&value)?),

            "lines" => {
                let value = value.parse::<usize>()?;
                ensure!(value != 0);
                lines = Some(value);
            }

            "unexpected" => {
                unexpected = state
                    .borrow()
                    .expected_messages
                    .as_ref()
                    .map(|expected_messages| expected_messages.patterns.clone());
                ensure!(unexpected.is_some());
            }

            key => bail!("Unexpected query parameter '{}'", key),
        }
    }

    let mut buffer = String::new();

    let cached_fields = {
        let state = state.borrow();

        for event in state.events.iter().rev() {
            if since.is_some() && since >= event.timestamp {
                continue;
            }

            if until.is_some() && until <= event.timestamp {
                continue;
            }

            if host.is_some() && host.as_deref() != event.host.as_deref() {
                continue;
            }

            if container.is_some() && container.as_deref() != event.container.as_deref() {
                continue;
            }

            if unit.is_some() && unit.as_deref() != event.unit.as_deref() {
                continue;
            }

            let mut message = None;

            if let Some(field) = state.fields.get("MESSAGE") {
                for field_value in &event.fields {
                    if field.id == field_value.id {
                        let mut value = &field.data[field_value.offset..];
                        let length = read_leb128(&mut value).try_into().unwrap();
                        let value = &value[..length];

                        message = Some(String::from_utf8_lossy(value));
                        break;
                    }
                }
            }

            let Some(message) = message else {
                continue;
            };

            if let Some(pattern) = &grep {
                if !pattern.is_match(&message) {
                    continue;
                }
            }

            let before_timestamp = buffer.len();

            let after_timestamp = format_event(
                &mut buffer,
                event.timestamp.unwrap(),
                event.host.as_deref(),
                event.container.as_deref(),
                event.unit.as_deref(),
                &message,
            );

            if let Some(patterns) = &unexpected {
                if patterns.is_match(&buffer[after_timestamp..]) {
                    buffer.truncate(before_timestamp);
                    continue;
                }
            }

            buffer.push('\n');

            if let Some(lines) = &mut lines {
                *lines -= 1;

                if *lines == 0 {
                    return Ok(buffer);
                }
            }

            if buffer.len() > RESPONSE_LIMIT {
                buffer.push_str(ADDITIONAL_MESSAGES);

                return Ok(buffer);
            }
        }

        state.cached_fields.clone()
    };

    spawn_blocking(move || {
        let mut conn = open_connection()?;
        let txn = conn.transaction()?;

        {
            let mut query = r#"
SELECT
    events.timestamp,
    hosts.name,
    containers.name,
    units.name,
    events.fields
FROM events
LEFT JOIN hosts ON events.host_id = hosts.id
LEFT JOIN containers ON events.container_id = containers.id
LEFT JOIN units ON events.unit_id = units.id
    "#
            .to_owned();

            let mut params = Vec::new();

            macro_rules! push_filter {
                ( $column:literal, $value:ident) => {
                    if let Some(value) = &$value {
                        query.push_str(if params.is_empty() {
                            concat!(" WHERE ", $column, " ?")
                        } else {
                            concat!(" AND ", $column, " ?")
                        });
                        params.push(value as &dyn ToSql);
                    }
                };
            }

            push_filter!("events.timestamp >", since);
            push_filter!("events.timestamp <", until);

            push_filter!("hosts.name =", host);
            push_filter!("containers.name =", container);
            push_filter!("units.name =", unit);

            query.push_str(" ORDER BY events.timestamp DESC");

            let mut stmt = txn.prepare_cached(&query)?;
            let mut rows = stmt.query(&params[..])?;

            let mut fields_stmt =
                txn.prepare_cached("SELECT data FROM fields WHERE id = ? AND name = 'MESSAGE'")?;

            'rows: while let Some(row) = rows.next()? {
                let timestamp: u64 = row.get(0)?;

                let host = row.get_ref_unwrap(1).as_str_or_null()?;
                let container = row.get_ref_unwrap(2).as_str_or_null()?;
                let unit = row.get_ref_unwrap(3).as_str_or_null()?;

                let mut fields = row.get_ref_unwrap(4).as_blob()?;

                'fields: while !fields.is_empty() {
                    let id = read_leb128(&mut fields);
                    let offset = read_leb128(&mut fields).try_into().unwrap();

                    let data = if let Some(data) = cached_fields.get(&id) {
                        data
                    } else {
                        let mut fields_rows = fields_stmt.query(params![id])?;

                        if let Some(fields_row) = fields_rows.next()? {
                            let data = fields_row.get_ref_unwrap(0).as_blob()?;

                            let data = decompress(data, usize::MAX)?;

                            let data = Arc::<[u8]>::from(data);
                            cached_fields.insert(id, data.clone());
                            data
                        } else {
                            continue 'fields;
                        }
                    };

                    let mut value = &data[offset..];
                    let length = read_leb128(&mut value).try_into().unwrap();
                    let value = &value[..length];

                    let message = String::from_utf8_lossy(value);

                    if let Some(pattern) = &grep {
                        if !pattern.is_match(&message) {
                            continue 'rows;
                        }
                    }

                    let before_timestamp = buffer.len();

                    let after_timestamp =
                        format_event(&mut buffer, timestamp, host, container, unit, &message);

                    if let Some(patterns) = &unexpected {
                        if patterns.is_match(&buffer[after_timestamp..]) {
                            buffer.truncate(before_timestamp);
                            continue 'rows;
                        }
                    }

                    buffer.push('\n');
                    break 'fields;
                }

                if let Some(lines) = &mut lines {
                    *lines -= 1;

                    if *lines == 0 {
                        break;
                    }
                }

                if buffer.len() > RESPONSE_LIMIT {
                    buffer.push_str(ADDITIONAL_MESSAGES);

                    break;
                }
            }
        }

        txn.commit()?;

        Ok(buffer)
    })
    .await
    .unwrap()
}

pub fn format_event(
    buffer: &mut String,
    timestamp: u64,
    host: Option<&str>,
    container: Option<&str>,
    unit: Option<&str>,
    message: &str,
) -> usize {
    write!(
        buffer,
        "{}",
        format_rfc3339_seconds(SystemTime::UNIX_EPOCH + Duration::from_secs(timestamp))
    )
    .unwrap();

    let after_timestamp = buffer.len() + 1;

    if let Some(part) = host {
        buffer.push(' ');
        buffer.push_str(part);
    }

    if let Some(part) = container.or(unit) {
        buffer.push(' ');
        buffer.push_str(part);
    }

    buffer.push_str(": ");
    buffer.push_str(message);

    after_timestamp
}

pub const ADDITIONAL_MESSAGES: &str =
    "\nThere are additional log messages exceeding the response limit.";
