use anyhow::{ensure, Result};

pub fn parse_timestamp(value: &[u8]) -> Result<u64> {
    ensure!(value.len() > 6);
    let value = &value[..value.len() - 6];

    let mut timestamp = 0;

    for byte in value {
        ensure!(byte.is_ascii_digit());
        timestamp = timestamp * 10 + u64::from(*byte - b'0');
    }

    Ok(timestamp)
}

const CONTINUATION_BIT: u8 = 1 << 7;

pub fn read_leb128(buffer: &mut &[u8]) -> u64 {
    let mut value = 0;
    let mut shift = 0;

    let mut consumed = 0;

    for _ in 0..10 {
        let byte = buffer[consumed];
        consumed += 1;

        value |= ((byte & !CONTINUATION_BIT) as u64) << shift;

        if byte & CONTINUATION_BIT == 0 {
            break;
        }

        shift += 7;
    }

    *buffer = &buffer[consumed..];

    value
}

pub fn write_leb128(buffer: &mut Vec<u8>, mut value: u64) {
    for _ in 0..10 {
        let mut byte = (value & (!CONTINUATION_BIT as u64)) as u8;

        value >>= 7;

        if value != 0 {
            byte |= CONTINUATION_BIT;
        }

        buffer.push(byte);

        if value == 0 {
            break;
        }
    }
}
