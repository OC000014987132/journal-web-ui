use std::cell::RefCell;
use std::env::var;
use std::fs::read_to_string;
use std::mem::take;
use std::sync::Arc;
use std::time::Duration;

use anyhow::Result;
use humantime::parse_duration;
use lettre::{
    message::{header::ContentType, Mailbox},
    AsyncSmtpTransport, AsyncTransport, Message, Tokio1Executor,
};
use regex::RegexSet;
use tokio::{
    pin, select,
    sync::{
        mpsc::{channel, unbounded_channel, Receiver, Sender, UnboundedReceiver},
        oneshot::Sender as OneshotSender,
    },
    task::spawn_local,
    time::{sleep, sleep_until, Instant},
};

use crate::{
    view_events::ADDITIONAL_MESSAGES, ExpectedMessages, State, RESPONSE_LIMIT, SEND_INTERVAL,
};

pub fn send_mail(state: &RefCell<State>) -> Result<Option<Sender<OneshotSender<()>>>> {
    let Ok(path) = var("EXPECTED_MESSAGES") else {
        return Ok(None);
    };

    let patterns = {
        let buffer = read_to_string(path)?;

        RegexSet::new(buffer.lines().filter(|line| !line.starts_with('#')))?
    };

    let send_interval = match var("SEND_INTERVAL") {
        Ok(send_interval) => parse_duration(&send_interval)?,
        Err(_err) => SEND_INTERVAL,
    };

    let mail_server = var("MAIL_SERVER")?;
    let mail_from = var("MAIL_FROM")?.parse()?;
    let mail_to = var("MAIL_TO")?.parse()?;

    let (flush_sender, mut flush_receiver) = channel(1);
    let (message_sender, mut message_receiver) = unbounded_channel();

    state.borrow_mut().expected_messages = Some(ExpectedMessages {
        patterns: Arc::new(patterns),
        sender: message_sender,
        curr_message: String::new(),
        buffer: String::new(),
    });

    spawn_local(async move {
        while let Err(err) = buffer_messages(
            send_interval,
            &mail_server,
            &mail_from,
            &mail_to,
            &mut flush_receiver,
            &mut message_receiver,
        )
        .await
        {
            eprintln!("Failed to send mail: {:#}", err);
            sleep(send_interval).await;
        }
    });

    Ok(Some(flush_sender))
}

async fn buffer_messages(
    send_interval: Duration,
    mail_server: &str,
    mail_from: &Mailbox,
    mail_to: &Mailbox,
    flush_receiver: &mut Receiver<OneshotSender<()>>,
    message_receiver: &mut UnboundedReceiver<String>,
) -> Result<()> {
    let send_at = sleep_until(Instant::now());
    pin!(send_at);

    let mut buffer = String::new();

    loop {
        select! {
            biased;

            () = &mut send_at, if !buffer.is_empty() => {
                clear_buffer(mail_server, mail_from, mail_to, &mut buffer).await?;
            }

            Some(flush_sender) = flush_receiver.recv(), if !buffer.is_empty() => {
                clear_buffer(mail_server, mail_from, mail_to, &mut buffer).await?;

                let _ = flush_sender.send(());
            }

            message = message_receiver.recv() => {
                if let Some(message) = message {
                    if buffer.len() > RESPONSE_LIMIT {
                        continue;
                    }

                    if buffer.is_empty() {
                        send_at.as_mut().reset(Instant::now() + send_interval);
                    }

                    if !buffer.is_empty() {
                        buffer.push_str("\r\n");
                    }

                    buffer.push_str(&message);

                    if buffer.len() > RESPONSE_LIMIT {
                        buffer.push('\r');
                        buffer.push_str(ADDITIONAL_MESSAGES);
                    }
                } else {
                    break;
                }
            }
        }
    }

    Ok(())
}

async fn clear_buffer(
    mail_server: &str,
    mail_from: &Mailbox,
    mail_to: &Mailbox,
    buffer: &mut String,
) -> Result<()> {
    println!("Sending unexpected log messages via mail...",);

    let mail = Message::builder()
        .from(mail_from.clone())
        .to(mail_to.clone())
        .subject("Unexpected log messages")
        .header(ContentType::TEXT_PLAIN)
        .body(take(buffer))?;

    AsyncSmtpTransport::<Tokio1Executor>::builder_dangerous(mail_server)
        .build()
        .send(mail)
        .await?;

    Ok(())
}
