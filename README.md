# journal-web-ui

A service which provides a web UI for the systemd journal.

Web-based access to systems logs can be useful as it avoids the requirement of SSH access to the host collecting the logs which is beneficial for the security of the system, as there are fewer access points.

The service also implements a custom engine for centrallized storage to deduplicate and index the low-cardinality fields host, container and unit as well as column-oriented compression for the other fields thereby minimizing the storage requirements for centrallized log collection.

## Installation

1. The CI here builds a ready-to-use Debian package availabl at <https://gitlab.opencode.de/api/v4/projects/628/jobs/artifacts/main/download?job=build>.
2. This package needs to be installed on the target machine and will listen for HTTP connections on port 19532.
3. Use `systemctl edit journal-web-ui.service` to set the following environment variables to configure the service:

| variable | description | example |
| -------- | ----------- | ------- |
| `EXPECTED_MESSAGES` | set of expected log messages (which are filtered out if requested) | `EXPECTED_MESSAGES=/etc/expected-log-messages` |
| `SEND_INTERVAL` | enables sending unexpected logs after the given number of seconds | `SEND_INTERVAL=5m` |
| `MAIL_SERVER` | server used to send unexpected logs to | `MAIL_SERVER=smtp-gateway` |
| `MAIL_FROM` | address from which mails are sent | `MAIL_FROM=journal@collector` |
| `MAIL_TO` | address to which mails are sent | `MAIL_TO=operations@company` |

## Usage

The service is used by making HTTP requests to port 19532, e.g. to <http://localhost:19532/>. It will respond with the logs as plain text. The set of display logs can be customised by adding one or more of the following query parameters:

| parameter | description | example |
| --------- | ----------- | ------- |
| `lines` | limit number of lines to be shown | `lines=100` |
| `host` | limits the output to the given hostname | `host=the-name-of-the-host` |
| `container` | limits the output ot the given container | `container=the-name-of-the-container` |
| `unit` | limits the output to the given systemd unit | `unit=systemd-journald.service` |
| `since` | defines the start date of the analysis | `since=-24h` |
| `until` | defines the end date of the analysis | `until=2023-10-19 12:00` |
| `grep` | filters the messages for given regular expression | `grep=ERROR` |
| `unexpected` | filters the messages using the configured set of expected log messages | `unexpected=` |

For example, to query all messages of `harvester.service` unit running on the `metadaten` machine logged in the last 24 hours and containing the pattern `ERROR`, use the following URL:

```
http://localhost:19532/?host=metadaten&unit=harvester.service&grep=ERROR&since=-24h
```

The configuration file specified by `EXPECTED_MESSAGES` defines which log messages are considered part of normal operations. The other, unexpected messages will regularly be sent to a configured mail address. Additionally, using the `unexpected` query parameter, the displayed log messages can be limited to the unexpected ones. This is similar to the functionality provided by the [`logcheck (8)`](https://manpages.ubuntu.com/manpages/jammy/en/man8/logcheck.8.html) utility.

The syntax of this file is line-oriented. Lines starting with a `#` are considered comments. Everything else is compiled as a regular expression following the syntax described at <https://docs.rs/regex/latest/regex/#syntax>.
